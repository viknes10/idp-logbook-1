<img src="https://gitlab.com/viknes10/idp-logbook-1/-/raw/main/R__1_.png" width=190 align=middle>

## Front cover 

| Heading | Information |
| ------ | ----------- |
| Log Entry Number   | Number 01 |
| Name | Vikneswaran A/L Devandran |
| Matric Number   | 197279 |
| Year of Study  | Year 4 |
| Subsystem Group | IDP Simulation |
| Team   | 1 |
| Date of Log   | 25/10/2021-29/10/2021 |

## Content of the Log

| Log Book Content | 
| ------ | 
| 1. Agenda   |
| 2. Goals   |
| 3. Decisions taken to solve problems   |
| 4. Method to solve problems   |
| 5. Impact of the decision taken into   |
| 6. Next progress or step   |

## Log Book 01 

1. Agenda:

- Having a brief discussions among the group members under that subsytem called simulations.

- Discussed the project outline or worload progress for the simulation group for the whole milestone until week 8.

- The simulation project outline was created and proposed through a modelled online based Gantt Chart.

- The simulation main task was proposed in the Gantt Chart are Real time measurements data collection, Catia Design Drawing, Flight Performamce Calculation and Computational Fluid Dynamics (ANSYS) analysis.

2. Goals:

- To take the reading or data collectioN regards to the exact measurements of physical HAU in the LAB.

- To have a simple ilustartions or the imagination on the HAU design especially of its airframe.

3. Problems:

- Have a task dividing problems in term of group member.

- The certain group member are still outside the campus.

- restrictions on the amount of students can enter the lab facility.

4. Decision:

- The group of ceratin students staying near campus was voluntered themselves in the group to do the first task.

5.  Method:

- The group member had been planned out the timing and do the confirmation with the person in charge of the Lab facility to have an access in recording the data measurements. 

6. Impact:

- The first task as been planned out in the timeline of the simulation progress which is the Real time data collection on the physical HAU had been fully confirmed.

7. Next Step:

- The real time measurements on the real physical HAU have to be taken.

